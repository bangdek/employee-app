import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ForgotPasswordComponent } from '../forgot-password/forgot-password.component';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../../service/auth.service';
import { ACCOUNT } from '../../../service/account';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent implements OnInit {

  // username: string = '9812341099'; // Variabel untuk menyimpan email
  // password: string = 'password';
  username = new FormControl(); 
  password = new FormControl();
  errorMessage: string = '';
  isLoading:boolean = false
  
  account : any[] = []
  constructor(
    private dialog: MatDialog,
    private _snackBar: MatSnackBar,
    private router: Router,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    if (this.authService.isLoggedIn()) {
      this.router.navigate(['/pages/dashboard']); // Redirect to protected route
    }
  }

  openForgotPasswordModal(): void {
    this.dialog.open(ForgotPasswordComponent);
  }


  onSubmit(): void {
    if(this.username.valid && this.password.valid){
      this.isLoading = true;
      setTimeout(() => {
        const account = ACCOUNT.find(acc => acc.username === this.username.value);
        if (this.authService.login(this.username.value, this.password.value)) {
          this.authService.setLoginStatus(true);
          localStorage.setItem('account', JSON.stringify(account));
          this._snackBar.open('Login successfully!', '', { duration: 1000 });
          this.router.navigate(['/pages/dashboard']);
        } else {
          this.errorMessage = 'Invalid username or password';
          this._snackBar.open(this.errorMessage, '', { duration: 1000 });
        }
        this.isLoading = false;
      }, 1000);
    }
  }


  togglePasswordVisibility() {
    throw new Error('Method not implemented.');
  }
}
