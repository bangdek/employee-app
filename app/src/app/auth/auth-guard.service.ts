import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { AuthService }  from '../../service/auth.service'; // Assuming your AuthService is in the 'service' folder

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean | UrlTree{

    if (this.authService.isLoggedIn()) {
      // Check if the current URL is the login page
      if (state.url.includes('/login')) {
        this.router.navigate(['/pages']);
        return false;
      } else {
        return true;
      }
    } else {
      // If not logged in, redirect to login
      this.router.navigate(['/login']);
      return false;
    }
  }
}
