import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';


@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrl: './forgot-password.component.scss'
})
export class ForgotPasswordComponent {

  constructor(private dialogRef: MatDialogRef<ForgotPasswordComponent>) {}
  
  adminName: string = 'Admin Human Capital';
  adminEmail: string = 'admin.hc@email.com';
  adminPhone: string = '+62 812-3456-7890';
  message: string = 'Mohon hubungi admin Human Capital untuk mendapatkan kembali akses login Anda. Informasi kontak admin adalah sebagai berikut:';
  closingMessage: string = 'Terima kasih atas pengertian dan kerjasamanya.';

  onClose(): void {
    this.dialogRef.close();
  }
}
