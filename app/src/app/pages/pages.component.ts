import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../service/auth.service';

@Component({
  selector: 'app-pages',
  templateUrl:'./pages.component.html' ,
  styleUrl: './pages.component.scss'
})
export class PagesComponent {
    fullname:string
    accountData :any = localStorage.getItem('account')
    constructor(private authService: AuthService, private router: Router) {
        const account = JSON.parse(this.accountData)
        // console.log(account)
        this.fullname = account?.firstName + ' ' + account?.lastName
    }

    logout(): void {
      this.authService.logout();
      this.router.navigate(['/login']);
    }
    isSidebarOpen: boolean = false;

    sidebarMenus: { label: string, icon: string, link: string }[] = [
        { label: 'Dashboard', icon: 'fa-dashboard', link: '/pages/dashboard' },
        { label: 'Employee List', icon: 'fa-user', link: '/pages/employee' },
        { label: 'Settings', icon: 'fa-cog', link: '/pages/settings' }
    ];

    toggleSidebar(): void {
        this.isSidebarOpen = !this.isSidebarOpen;
    }
}
