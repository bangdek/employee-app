import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Employee, Options } from '../../../service/employee.model';
import { EmployeeService } from '../../../service/employee.service';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { ModalEmployeeComponent } from './modal-employee/modal-employee.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EMPLOYEE } from '../../../service/employees';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrl: './employee.component.scss'
})
export class EmployeeComponent implements OnInit,AfterViewInit {
  displayedColumns: string[] = ['actions','username', 'firstName', 'lastName', 'email', 'birthDate', 'basicSalary', 'status', 'group', 'description'];
  dataSource = new MatTableDataSource<Employee>();
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  @ViewChild(MatSort)
  sort!: MatSort; 
  searchFirstName: string = '';
  searchLastName: string = '';
  groups: string[] = [];
  totalCategories: number = 0
  constructor(
    private employeeService: EmployeeService,
    private dialog: MatDialog,
    private _snackBar: MatSnackBar,
  ) {
    // const employees = EMPLOYEE
    // this.dataSource = new MatTableDataSource(employees)
  }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.loadEmployees();
    this.employeeService.getEmployeesFromLocalStorage()

    EMPLOYEE.forEach(employee => {
      const group = employee.group;
      if (!this.groups.includes(group)) {
        this.groups.push(group);
      }
    });

    this.totalCategories = this.groups.length;

  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  loadEmployees(): void {
    this.dataSource.data = this.employeeService.getEmployeesFromLocalStorage();
  }

  applyFilter(): void {
    this.dataSource.filterPredicate = (data: Employee, filter: string) => {
      const searchTerms = JSON.parse(filter);
      return data.firstName.toLowerCase().includes(searchTerms.firstName) &&
            data.lastName.toLowerCase().includes(searchTerms.lastName);
    };

    const filterValue = JSON.stringify({ firstName: this.searchFirstName.toLowerCase(), lastName: this.searchLastName.toLowerCase() });
    this.dataSource.filter = filterValue;

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openModalAdd(): void {
    const dialogRef = this.dialog.open(ModalEmployeeComponent);

    dialogRef.afterClosed().subscribe((result: Employee | undefined) => {
      if (result) {
        this._snackBar.open('Employee add successfully', '', {
          duration: 2000,
        });
        this.loadEmployees();
      }
    });
  }

  openModalEdit(item: Employee): void {
    const dialogRef = this.dialog.open(ModalEmployeeComponent, {
      data: { 
        employee: item,
        isEdit: true
      }
    });

    dialogRef.afterClosed().subscribe((result: Employee | undefined) => {
      if (result) {
        this._snackBar.open('Employee updated successfully', '', {
          duration: 2000,
        });
        this.loadEmployees();
      }
    });
  }

  deleteItem(username: string): void {
    this.employeeService.deleteEmployee(username);
    this._snackBar.open('Employee deleted successfully', '', {
      duration: 2000,
      panelClass: ['snackbar-red']
    });
    this.loadEmployees();
  }
}
