import { EmployeeService } from './../../../../service/employee.service';
import { Component, Input,Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Employee } from '../../../../service/employee.model';
import { EMPLOYEE } from '../../../../service/employees';

@Component({
  selector: 'app-modal-employee',
  templateUrl: './modal-employee.component.html',
  styleUrls: ['./modal-employee.component.scss']
})
export class ModalEmployeeComponent implements OnInit {
  employee: Employee | undefined;
  @Input() isEdit: boolean = false;
  groups: string[] = [];
  status: string[] = [];
  employeeForm!: FormGroup;

  constructor(
    private dialogRef: MatDialogRef<ModalEmployeeComponent>,
    private fb: FormBuilder,
    private EmployeeService: EmployeeService,
    @Inject(MAT_DIALOG_DATA) public data :any
  ) {
    if (data) {
      this.isEdit = data.isEdit;
      this.employee = data.employee;
    }
    this.initform()
  }

  ngOnInit(): void {
    // console.log(this.isEdit,this.employee)
    if(this.isEdit){
      this.employeeForm.patchValue({
        username: this.employee?.username,
        firstName: this.employee?.firstName,
        lastName : this.employee?.lastName,
        email: this.employee?.email,
        birthDate: this.employee?.birthDate,
        basicSalary: this.employee?.basicSalary,
        status: this.employee?.status,
        group: this.employee?.group,
        description: this.employee?.description
      })
    }

    EMPLOYEE.forEach(employee => {
      const group = employee.group;
      if (!this.groups.includes(group)) {
        this.groups.push(group);
      }
    });
    EMPLOYEE.forEach(employee => {
      const status = employee.status;
      if (!this.status.includes(status)) {
        this.status.push(status);
      }
    });

  }

  initform():void {
    this.employeeForm = this.fb.group({
      username: [this.employee?.username || '', Validators.required],
      firstName: [this.employee?.firstName || '', Validators.required],
      lastName: [this.employee?.lastName || '', Validators.required],
      email: [this.employee?.email || '', [Validators.required, Validators.email]],
      birthDate: [this.employee?.birthDate || '', Validators.required],
      basicSalary: [this.employee?.basicSalary || '', [Validators.required, Validators.min(0)]],
      status: [this.employee?.status || '', Validators.required],
      group: [this.employee?.group || '', Validators.required],
      description: [this.employee?.description || '', Validators.required]
    });
  }

  onSubmit(): void {
    if (this.employeeForm.valid) {
      // Handle form submission
      const employeeData: Employee = this.employeeForm.value;
      
      this.isEdit ? this.EmployeeService.editEmployee(employeeData.username , employeeData) : this.EmployeeService.addEmployee(employeeData)
      
      this.dialogRef.close(employeeData);
    }
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
