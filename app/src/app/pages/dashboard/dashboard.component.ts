import { Component } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.scss'
})
export class DashboardComponent {
  accountData :any = localStorage.getItem('account')
  fullname: string = ''
  constructor(){
    const account = JSON.parse(this.accountData)
    // console.log(account)
    this.fullname = account?.firstName + ' ' + account?.lastName
  }
}
