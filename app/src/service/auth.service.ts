import { Injectable } from '@angular/core';
import { ACCOUNT } from './account';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
    private loggedIn: boolean = false; 
    private loggedInStatus = JSON.parse(localStorage.getItem('loggedIn') || ('false'));
    private readonly LOGIN_KEY = 'isLoggedIn';

    constructor(){
        this.loggedIn = this.checkLoginStatus();
    }

    private checkLoginStatus(): boolean {
        return localStorage.getItem(this.LOGIN_KEY) === 'true';
    }
    
    setLoginStatus(value :boolean){
        this.loggedInStatus = value
        localStorage.setItem('isLoggedIn', 'true')
    }

    get LoginStatus() {
        return JSON.parse(localStorage.getItem('loggedIn') || 
        this.loggedInStatus.toString());
    }

    login(username: string, password: string): boolean {
        const user = ACCOUNT.find(emp => emp.username === username && password === emp.password);
        if (user) {
        this.loggedIn = true;
        return true;
        }
        return false;
    }

    logout(): void {
        this.loggedIn = false;
        localStorage.clear()
    }

    isLoggedIn(): boolean {
        return this.loggedIn;
    }
}
