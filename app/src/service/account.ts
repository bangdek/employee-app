export const ACCOUNT: any[] = [
    {
      username: '9812341099',
      password : 'admin',
      firstName: 'John',
      lastName: 'Doe',
      email: 'john.doe@example.com',
      birthDate: new Date('1998-01-01'),
      basicSalary: 50000,
      status: 'Active',
      group: 'Engineering',
      description: '2024-06-11',
    },
    {
      username: '9938651023',
      password : 'admin',
      firstName: 'Jane',
      lastName: 'Smith',
      email: 'jane.smith@example.com',
      birthDate: new Date('1999-05-15'),
      basicSalary: 60000,
      status: 'Active',
      group: 'Marketing',
      description: '2024-06-11',
  }
];