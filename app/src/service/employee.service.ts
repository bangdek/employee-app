import { Injectable } from '@angular/core';
import { Employee } from './employee.model';
import { EMPLOYEE } from './employees';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  private readonly EMPLOYEES_KEY = 'employees';

  constructor() { }

  // Ambil data karyawan dari local storage
  public getEmployeesFromLocalStorage(): Employee[] {
    const employeesString = localStorage.getItem(this.EMPLOYEES_KEY);
    return employeesString ? JSON.parse(employeesString) : [];
  }

  // Simpan data karyawan ke local storage
  private saveEmployeesToLocalStorage(employees: Employee[]): void {
    localStorage.setItem(this.EMPLOYEES_KEY, JSON.stringify(employees));
  }

  // Tambah karyawan baru
  addEmployee(employee: Employee): void {
    const employees = this.getEmployeesFromLocalStorage();
    employees.push(employee);
    this.saveEmployeesToLocalStorage(employees);
  }

  // Edit data karyawan
  editEmployee(username: string, updatedEmployee: Employee): void {
    let employees = this.getEmployeesFromLocalStorage();
    employees = employees.map(emp => emp.username === username ? updatedEmployee : emp);
    this.saveEmployeesToLocalStorage(employees);
  }

  // Hapus karyawan
  deleteEmployee(username: string): void {
    let employees = this.getEmployeesFromLocalStorage();
    employees = employees.filter(emp => emp.username !== username);
    this.saveEmployeesToLocalStorage(employees);
  }

  // Dapatkan daftar karyawan
  getEmployees(): Employee[] {
    this.saveEmployeesToLocalStorage(EMPLOYEE);
    return EMPLOYEE
  }
}
