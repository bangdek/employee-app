export interface Employee {
    username: string;
    firstName: string;
    lastName: string;
    email: string;
    birthDate: string;
    basicSalary: number;
    status: string;
    group: string;
    description: string;
}

export interface Options {
    code : string,
    value : string
}